"use strict";
// https://devhoangkien.com/memoization-code-performance-8ee64e1978b3

function memoize(func) {
  const cache = {};
  return function (...args) {
    const key = JSON.stringify(args);
    if (cache[key]) {
      return cache[key];
    } else {
      const result = func.apply(this, args);
      cache[key] = result;
      return result;
    }
  };
}

const fibonacci = memoize(function (n) {
  if (n <= 1) {
    return n;
  } else {
    return fibonacci(n - 1) + fibonacci(n - 2);
  }
});

// with new value
console.time("Execution Time");
console.log(fibonacci(500));
console.timeEnd("Execution Time");

// It's cached now
console.time("Execution Time Cached");
console.log(fibonacci(500));
console.timeEnd("Execution Time Cached");

// with new value
console.time("Execution Time");
console.log(fibonacci(170));
console.timeEnd("Execution Time");

// It's cached now
console.time("Execution Time Cached");
console.log(fibonacci(500));
console.timeEnd("Execution Time Cached");

// It's cached now
console.time("Execution Time Cached");
console.log(fibonacci(170));
console.timeEnd("Execution Time Cached");

console.time("Execution Time Cached");
console.log(fibonacci(170));
console.timeEnd("Execution Time Cached");
